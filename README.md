# DemoBwl

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

Elementos que componen el fronted


-  Menú 
o Este sólo hará que cierres sesión. 

-  Analizar humanos 
o Este deberá utilizar un servicio que sea capaz de contabilizar los humanos  registrados en base de datos que tiene mutación y cuales no tienen mutación. La  información será mostrada junto al texto “ADN detector” y al final deberá verse de  la siguiente manera: “ADN detector CM: 2 SM: 4” (CM: con mutación, SM: sin  mutación), donde los números pueden variar según el resultado en base de datos 

-  Con mutación 
Cuando esté activo solo mostrará las tarjetas de los humanos que tienen mutación o Entre el botón “Con mutación” y “Sin mutación” solo se puede activar uno a la vez. 

-  Sin mutación 

Cuando esté activo solo mostrará las tarjetas de los humanos que NO tienen  mutación 
Entre el botón “Con mutación” y “Sin mutación” solo se puede activar uno a la vez. 


-  Nuevo humano 
Este botón hará que se muestre un formulario para crear un humano
La pantalla del nuevo humano deberá crear un humano e  internamente generar su código ADN de forma aleatoria en un servicio del backend, al  crearse se deberá actualizar la pantalla principal con ese nuevo humano. 


para realizar el despliegue en local hay que ejecutar los siguientes comandos dentro del proyecto descargado:

1. npm install
2. ng serve

Para realizar el despliegue en un servidor tomcat en una maquina virtual de google se deben realizar las siguientes acciones:
1. crear una vm en goole siguiendo los pasos descritos en la documentación oficial ([https://cloud.google.com/compute/docs/quickstart-linux?hl=es-419](url))
2. habilitar el puesto de conexión que sera utilizado en el firewall de vpc google cloud
3. una vez habilitada la conexión al puerto se debe entrar a la vm via ssh mediante la consola con gcloud o la plataforma de google cloud
4. dentro de la consola de la maquina virtual de google cloud se debe ejecutar los siguientes comandos 
   -   sudo apt install default-jdk (comando para instalar el jdk necesario para  tomcat)
   -   sudo groupadd tomcat (grupo creado para encapsular la ejecución de tomcat y no vulnerar el root de la vm)
   -   sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat (Creación de un usuario Tomcat para que en el servidor se tenga la posibilidad de creación de archivos)
5. sudo mkdir /opt/tomcat (creación de directorio de ejecución)
6. cd /tmp curl -O http://mirrors.estointernet.in/apache/tomcat/tomcat-9/v9.0.34/bin/apache-tomcat-9.0.34.tar.gz
sudo tar xzvf apache-tomcat-9.0.34.tar.gz -C /opt/tomcat --strip-components=1 (comando para la descarga de tomcat, nota si no se     encuentra esta versión de tomcat se reemplaza por cualquier otra)
7. cd /opt/tomcat se entra al directorio de trabajo
8. sudo chgrp -R tomcat /opt/tomcat
   sudo chmod -R g+r conf
   sudo chmod g+x conf
   sudo chown -R tomcat webapps/ work/ temp/ logs/ (comandos para dar permisos al usuario tomcat que creamos)

tras realizar estas descargas se realzia la configuración de tomcat y un servicio para facilitar el detener y arrancar de nuestro tomcat

9.  sudo nano /etc/systemd/system/tomcat.service (comando que crea el archivo que sera el servicio)
 ingresar el contenido del archivo

 [Unit]
Description=Apache Tomcat Web Application Container
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target

tras ingresar este contenido se presiona las teclas "ctrl" + "x" y "enter"

y actualizamos los servicios cargados por linux con
11. sudo systemctl daemon-reload

despues de  que se ejecute el comando anterior 

se pueden ejecutar los siguientes comandos:
sudo systemctl start tomcat y sudo systemctl enable tomcat

para configurar nuestro usuario de tomcat se debe ejecutar el siguiente comando en la vm

sudo nano /opt/tomcat/conf/tomcat-users.xml

ingresando la siguiente linea dentro de la etiqueta "tomcat-users"
    <user username="admin" password="password" roles="manager-gui,admin-gui"/>


posteriormente hay que considerar que por defecto tomcat no permite la conexión de ips externas al servidor en el que se encuentre por tal motivo hay que cambiar eso comentando la siguiente linea de los archivos que estan ubicados en "/opt/tomcat/webapps/manager/META-INF/context.xml" y "/opt/tomcat/webapps/host-manager/META-INF/context.xml"

<Valve className="org.apache.catalina.valves.RemoteAddrValve"
         allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />

ejecutando todo lo anterior el servidor quedara cconfigurado para su uso


Para el despliegue de nuestro codigo de angular en tomcat se debe ejecutar el comando 

ng build --prod --base-href /bwl-demo/

con lo cual se construira nuestra carpeta dist. posteriormente hay que copiar nuestra dist mediante ssh a nuestra vm en google 
y en la ruta /opt/tomcat/webapps crear una carpeta con el nombre bwl-demo y dentro de ella poner el contenido que se encontraba dentro de la carptea que se encontraba en nuestra carpeta dist

"nota" todo lo anterior se debe hacer considerando que nuestro api base registrado en environments de angular tiene que ser el mismo que tenga nuestro back-end  de lo contrario no podran conectarse










