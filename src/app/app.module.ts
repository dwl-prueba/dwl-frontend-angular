import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { app_routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './componentes/home/home.component';
import { NavbarComponent } from './componentes/compartido/navbar/navbar.component';
import { BotonSalidaComponent } from './componentes/compartido/boton-salida/boton-salida.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { LoginComponent } from './componentes/compartido/login/login.component';
import { TarjetaLoginComponent } from './componentes/compartido/tarjeta-login/tarjeta-login.component';
import { GuardAutentication } from './componentes/compartido/interceptores/guardAutentication.interceptor';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderInterceptor } from './componentes/compartido/interceptores/headerinterceptor.service';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    BotonSalidaComponent,
    LoginComponent,
    TarjetaLoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    app_routing,
    BrowserAnimationsModule
  ],
  exports:[
  ],
  providers: [GuardAutentication,FormBuilder, {provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
