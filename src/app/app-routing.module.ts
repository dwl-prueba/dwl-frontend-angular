import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuardAutentication } from './componentes/compartido/interceptores/guardAutentication.interceptor';
import { LoginComponent } from './componentes/compartido/login/login.component';
import { HomeComponent } from './componentes/home/home.component';


const APP_ROUTES: Routes = [
  
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent,canActivate:[GuardAutentication]},
  {path: '', redirectTo: 'login', pathMatch: 'full'}

];

export const app_routing = RouterModule.forRoot(APP_ROUTES)