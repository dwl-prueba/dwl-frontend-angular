import { Component, OnInit, ViewChild } from '@angular/core';
import { Persona } from '../compartido/modelos/persona';
import { NavbarComponent } from '../compartido/navbar/navbar.component';
import { PersonaService } from '../compartido/servicios/humano.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConteoMutacion } from '../compartido/modelos/conteo-mutacion';
import { Subject } from 'rxjs';
import { MENSAJES } from '../compartido/mensajes';
import { debounceTime } from 'rxjs/operators';
import { Globales } from '../compartido/constantes/contantes';
declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild(NavbarComponent) barraNavegacion;
  public personas: Persona[];
  public opcionSeleccionada: number;
  public inputcss: boolean;
  public HumanoForm:FormGroup;
  public conteoMutacion:ConteoMutacion;
  private error: Subject<string>;
  public mensajeError: string;

  constructor(
    public fb: FormBuilder,
    private personaService: PersonaService) {
    this.opcionSeleccionada = -1;
    this.error = new Subject<string>();
    this.personas = [];
    this.inputcss = false;
  }

  ngOnInit(): void {
    this.inputcss = true;
    this.getPersonas();
    this.HumanoForm = this.fb.group({
      nombre: ['', Validators.required]
    });
    this.error.pipe(debounceTime(Globales.tiempoMensaje)).subscribe(() => this.mensajeError = null);
    this.error.subscribe((message) => {
      this.mensajeError = message;
      window.scroll(0, 0);
    });

  }
  eventosNavBar(evento) {
    console.log(evento)
    switch (evento) {
      case 0:
        this.opcionSeleccionada = 0;
        this.HumanoForm.controls.nombre.setValue(null);
        $("#modalPersona").modal("show");
        break;
      case 1:
        this.getConteo();
        break;
      case 2:
        this.getPersonasPorMutacion(true);
        break;
      case 3:
        this.getPersonasPorMutacion(false);
        break;

    }

  }

  getPersonas() {
    this.personaService.getPersonasGeneral().subscribe(response => {
      this.personas = response;
      console.log(this.personas);
      
    })
  }
  getPersonasPorMutacion(mutacion: boolean) {
    if (mutacion) {
      this.opcionSeleccionada = 2;
    } else {
      this.opcionSeleccionada = 3;
    }
    this.personaService.getPersonasPorMutacion(mutacion).subscribe(response => {
      this.personas = response;
    })
  }
  getConteo() {
    this.personaService.getConteo().subscribe(response => {
      this.conteoMutacion =response;
      this.opcionSeleccionada = 1;
      console.log(this.conteoMutacion)
    })
  }
  guardarHumano() {
    if(this.HumanoForm.valid){

      let HUMANO_NUEVO = { nombre: this.HumanoForm.controls.nombre.value };
      this.personaService.saveHumano(HUMANO_NUEVO).subscribe(response => {
        switch (this.opcionSeleccionada) {
          case 0:
            this.getPersonas();
            
            break;
          case 2:
            this.getPersonasPorMutacion(true);
            break;
          case 3:
            this.getPersonasPorMutacion(false);
            break;
        }
        if(this.conteoMutacion!= null){
          this.getConteo();
        }
        $("#modalPersona").modal("hide");

      })
    }{
this.error.next(MENSAJES.MSJ003);
    }
    

  }
}
