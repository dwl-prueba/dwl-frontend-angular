import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AutenticacionService } from '../servicios/autenticacion.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

    constructor(private autenticacionService: AutenticacionService, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        catchError(e => {
            if (e.status == 403) {
                if (this.autenticacionService.isAuthenticated()) {
                    this.autenticacionService.logout();
                    console.log('se cerró la sesión, el token ha expirado')
                }
                this.router.navigate(['/login']);

            }

            return throwError(e);
        });
        
        if (req.body instanceof FormData) {
            const header = req.clone({
                headers: req.headers.set('authorization', `Bearer ${this.autenticacionService.token}`)
            });
            return next.handle(header);
        } else {
            const header = req.clone({
                headers: req.headers.set('Content-Type', 'application/json').set('authorization', `Bearer ${this.autenticacionService.token}`)
            });
            return next.handle(header);
        }
    }
}
