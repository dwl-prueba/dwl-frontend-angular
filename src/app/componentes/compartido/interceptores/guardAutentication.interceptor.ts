
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { AutenticacionService } from '../servicios/autenticacion.service';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
  })
  export class GuardAutentication implements CanActivate {
    public state: RouterStateSnapshot;
    constructor(public authService: AutenticacionService, private router: Router) {
    }
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authService.isAuthenticated()) {
            //token expirado
            if (this.isTokenExpirado()) {
                this.authService.logout();
                this.router.navigate(['/login']);
                return false;
            }
            //fin token expirado
            return true;
        }
        this.router.navigate(['/login']);  
       return false;
    }


    isTokenExpirado(): boolean {
        let token = this.authService.token;
        let payload = this.authService.obtenerDatosToken(token);
        let now =new Date().getTime() / 1000;
        if (payload.exp < now) {
            return true;
        }
        return false;
    
      }
}
