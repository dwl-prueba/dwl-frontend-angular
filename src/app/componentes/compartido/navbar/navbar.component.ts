import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { ConteoMutacion } from '../modelos/conteo-mutacion';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  @Output()
  eventosNavBar = new EventEmitter<number>();
  registrosEnConteo:boolean;
  conteoMutacion:ConteoMutacion;
  @Input() set ConteoMutacionInput(conteoMutacion: ConteoMutacion) {
    this.conteoMutacion = conteoMutacion;
    
    if(conteoMutacion != null){
      this.registrosEnConteo = true;
    }
    
  }
  @Input() set botonSeleccionado(botonSeleccionado:number) {
    this.botonactivo = botonSeleccionado;
  }
  
  botonactivo:number;
  constructor() { 
    this.registrosEnConteo =false;
    this.botonactivo = -1
  }

  ngOnInit(): void {
    $('.hamburger').click(function(){
      $(this).toggleClass("is-active");
    });
  }
nuevoHumano(){
  this.botonactivo = 0;
  this.eventosNavBar.emit(0);
  
}
AnalizarHumano(){
  this.botonactivo = 1;
  this.eventosNavBar.emit(1);
  
}
getHumanosConMutacion(){
  this.botonactivo = 2;
  this.eventosNavBar.emit(2);
  
}
getHumanosSinMutacion(){
  this.botonactivo = 3;
  this.eventosNavBar.emit(3);
  
}

}
