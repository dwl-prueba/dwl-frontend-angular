import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticacionService } from '../servicios/autenticacion.service';

@Component({
  selector: 'app-boton-salida',
  templateUrl: './boton-salida.component.html',
  styleUrls: ['./boton-salida.component.css']
})

export class BotonSalidaComponent implements OnInit {
  @ViewChild('botonCerrarSesion') botonCerrarSesion: ElementRef;
  constructor(public autenticacionService: AutenticacionService,
    public router: Router) { }

  ngOnInit(): void {
    
  }

  despliegeBtnCierreSesion(){
    this.botonCerrarSesion.nativeElement.classList.toggle('change');
    this.autenticacionService.logout();
    this.router.navigate(['/'], { replaceUrl: true });
  }
  

}
