import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AutenticacionService } from '../servicios/autenticacion.service';
import { VerificaLogeoService } from '../servicios/verifica-logeo.service';
import {TarjetaLoginComponent}  from '../tarjeta-login/tarjeta-login.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild(TarjetaLoginComponent) tarjetaLogin;
  loginForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public autenticacionService: AutenticacionService,
    public router: Router,
    private verificaLogeoService: VerificaLogeoService ,

  ) { }

  ngOnInit(): void {
    if (this.autenticacionService.isAuthenticated()) {
      this.router.navigate(['/home'], { replaceUrl: true });
    }
    this.loginForm = this.fb.group({
      usuario: ['', Validators.required],
      contrasenia: ['', Validators.required]
    });
  }

  onSubmit() {
    if(this.loginForm.valid){
    this.autenticacionService.login(this.loginForm.controls.usuario.value,this.loginForm.controls.contrasenia.value ).subscribe(response => {
      this.autenticacionService.guardarCredenciales(response);
      
      this.verificaLogeoService.setUserLoggedIn(true);
      this.router.navigate(['/bienvenidos'], { replaceUrl: true });
    },
      err => {
        console.log(err);
        if (err.status == 401) {
console.log("error");
        }
        /*else if(err.status == 409){
          this.mensajeError = MENSAJES.MSJ055;
        }*/
      });
    }else{
     console.log("error de formato")
    }


  }}
