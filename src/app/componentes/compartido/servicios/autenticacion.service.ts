import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../modelos/usuario';

@Injectable({
    providedIn: 'root'
})
export class AutenticacionService {

    public httpHeaders = new HttpHeaders();
    private urlEndPoint = environment.APIEndpoint + 'api/login';
    public roles: string[] = [];
    public _usuario: any;
    public _token: string;
    public laboratorio: string;

    constructor(private http: HttpClient) {
        this.httpHeaders = this.httpHeaders.append("Content-Type", "application/json");
    }

    public get usuario(): Usuario {
        if (this._usuario != null) {
            return this._usuario;
        } else if (this._usuario == null && sessionStorage.getItem('usuario') != null) {
            this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
            return this._usuario;
        }
        return null;
    }

    public get token(): string {
        if (this._token != null) {
            return this._token;
        } else if (this._token == null && sessionStorage.getItem('token') != null) {
            this._token = sessionStorage.getItem('token');
            return this._token;
        }
        return null;
    }


    login(usuario: string, contrasenia: string): Observable<any> {
        var formData: any = new FormData();
        formData.append("username", usuario);
        formData.append("password", contrasenia);
        
        return this.http.post<any>(this.urlEndPoint,formData);
    }

    guardarCredenciales(response: any): void {
        this._token = response.token;
        sessionStorage.setItem('usuario', JSON.stringify(response.usuario));
        sessionStorage.setItem('token', response.token);
    }
    logout(): void {
        sessionStorage.clear();
        this._usuario = null;
        this._token = null;
    }
    obtenerDatosToken(accessToken: string): any {
        if (accessToken != null) {
            return JSON.parse(atob(accessToken.split(".")[1]));
        }
        return null;
    }

    isAuthenticated(): boolean {
        this._token = sessionStorage.getItem('token');
        let payload = this.obtenerDatosToken(this._token);

        if (payload != null && payload.sub && payload.sub.length > 0) {
            return true;
        }
        return false;
    }
}