import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Persona } from '../modelos/persona';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConteoMutacion } from '../modelos/conteo-mutacion';

@Injectable({
    providedIn: 'root'
})
export class PersonaService {
    private endPoint = environment.APIEndpoint + "persona";
    constructor(private http: HttpClient) {
    }
    public getPersonasGeneral(): Observable<Persona[]> {
        return this.http.get<Persona[]>(this.endPoint + "/todas").pipe(
            catchError(
                error => {
                    return throwError(error);
                })
        )
    }
    public getPersonasPorMutacion(mutacion:boolean): Observable<Persona[]> {
        return this.http.get<Persona[]>(this.endPoint + `/mutacion/${mutacion}`).pipe(
            catchError(
                error => {
                    return throwError(error);
                })
        )
    }
    public getConteo(): Observable<ConteoMutacion> {
        return this.http.get<ConteoMutacion>(this.endPoint + `/mutacion/conteo`).pipe(
            catchError(
                error => {
                    return throwError(error);
                })
        )
    }

    public saveHumano(cuerpo:any): Observable<any> {
        return this.http.post(`${this.endPoint}/`, cuerpo).pipe(
          catchError(
            error => {
              return throwError(error);
            }
          )
        );
      }
}