import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { error } from 'protractor';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Globales } from '../constantes/contantes';
import { MENSAJES } from '../mensajes';
import { AutenticacionService } from '../servicios/autenticacion.service';
import { VerificaLogeoService } from '../servicios/verifica-logeo.service';



@Component({
  selector: 'app-tarjeta-login',
  templateUrl: './tarjeta-login.component.html',
  styleUrls: ['./tarjeta-login.component.css']
})
export class TarjetaLoginComponent implements OnInit {
  @Output() autenticacion = new EventEmitter<boolean>();
  loginForm: FormGroup;
  private error: Subject<string>;
  public mensajeError: string;

  constructor(
    public fb: FormBuilder,
    public autenticacionService: AutenticacionService,
    public router: Router,
    private verificaLogeoService: VerificaLogeoService,
  ) {
    this.error = new Subject<string>();
   }
  ngOnInit(): void {
    this.error.pipe(debounceTime(Globales.tiempoMensaje)).subscribe(() => this.mensajeError = null);

    if (this.autenticacionService.isAuthenticated()) {
      this.router.navigate(['/home'], { replaceUrl: true });
    }
    this.loginForm = this.fb.group({
      usuario: ['', Validators.required],
      contrasenia: ['', Validators.required]
    });
    this.error.subscribe((message) => {
      this.mensajeError = message;
      window.scroll(0, 0);
    });
  }
  onSubmit() {
    if(this.loginForm.valid){
    this.autenticacionService.login(this.loginForm.controls.usuario.value,this.loginForm.controls.contrasenia.value).subscribe(response => {
      this.autenticacionService.guardarCredenciales(response);
      this.verificaLogeoService.setUserLoggedIn(true);
      this.router.navigate(['/home'], { replaceUrl: true });
    },
      err => {
        console.log(err);
        if (err.status == 401) {
          this.error.next(MENSAJES.MSJ001)
        }
        if (err.status == 403) {
          console.log("error");
        }
        
      });
    }else{
      this.error.next(MENSAJES.MSJ002)
    }

  }

}
